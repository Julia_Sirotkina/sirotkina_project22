//
//  ViewController.swift
//  pogoda2
//
//  Created by WSR on 6/20/19.
//  Copyright © 2019 WSR. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {
    var timezone = 0
    @IBOutlet weak var imageweather: UIImageView!
    @IBOutlet weak var dateTime: UILabel!
    @IBOutlet weak var fin: UILabel!
    @IBOutlet weak var tempCity: UILabel!
    @IBOutlet weak var imageFon: UIImageView!
    var city = "Moscow"
    var userDefaults = UserDefaults()
    
 var cityMain = "Екатеринбург"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fin.text = cityMain
        downloadData(city: cityMain)
        imageFon.image = UIImage(named: "png-clouds-2")
       
    }
    func downloadData(city: String) {
        let token = "1e936ee21707e2a418e98dca00877357"
        let urlStr = "https://api.openweathermap.org/data/2.5/weather?q=\(city)&units=metric&appid=\(token)"
        let url = urlStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        Alamofire.request(url!, method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                self.tempCity.text = json["main"]["temp"].stringValue+"°C"
                self.timezone = json["timezone"].intValue
            //                print(self.temp)
                //создаем переменную icon/ храним в ней массив []
              let icon  = json["weather"][0] ["icon"].stringValue
              let urlStringIcon = "https://api.openweathermap.org/img/w/\(icon).png"
              let urlIcon = URL(string: urlStringIcon)
              let dateIcon = try! Data(contentsOf: urlIcon! as URL)
              self.imageweather.contentMode = .scaleToFill
              self.imageweather.image = UIImage(data: dateIcon as! Data)
                self.Time()
            
            case .failure(let error):
                print(error)
            }
        }

    }
    //время
    func Time() {
        
        let date = Date()
        //получение таймзоны с сервера
        let timezonetime: TimeZone = TimeZone(secondsFromGMT: self.timezone )!
        //объявление фоматирования даты
        let dateFormatterPrint = DateFormatter()
        //формат времени
        dateFormatterPrint.dateFormat = "HH:mm"
        //формат таймзоны
        dateFormatterPrint.timeZone = timezonetime
        //вывод в dateTime
        dateTime.text = dateFormatterPrint.string(from: date)
    }
}
